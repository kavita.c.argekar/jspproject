package com.alfj.demo.services;

import com.alfj.demo.beans.User;
import com.alfj.demo.repository.UserRepository;

public class UserService {
	
	private UserRepository repo;
	
	public UserService() {
		repo= new UserRepository();
	}
	
	public boolean validateUser(User user) {
		
		return repo.validateUser(user);
	}

}
