package com.alfj.demo.servlet;

import java.io.IOException;

import com.alfj.demo.beans.User;
import com.alfj.demo.services.UserService;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

//@WebServlet("/login")
public class UserServlet extends HttpServlet {

	private UserService service;

	public UserServlet() {
		System.out.println("inside constructor");
		service = new UserService();

	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside get method");
		String userId = req.getParameter("userid");

		String password = req.getParameter("password");
		System.out.println("user id " + userId + " and password is " + password);
		User user = new User();
		user.setUserId(userId);
		user.setPassword(password);
		if (service.validateUser(user)) {
			RequestDispatcher rd = req.getRequestDispatcher("Welcome.jsp");
			req.setAttribute("id", userId);
			rd.forward(req, resp);
		} else {
			RequestDispatcher rd = req.getRequestDispatcher("Login.jsp");
			req.setAttribute("error", "invalid credential");
			rd.forward(req, resp);
		}

	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("inside post method");

		doGet(req, resp);
	}
}
